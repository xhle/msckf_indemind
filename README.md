# msckf_indemind

* image 25 hz, imu 200 hz
* need to start sensor from static
* indemind distortion is equidistant, not radtan
* output should change to rad/s from degree/s, m/s^2 from g. The output is same with realsense output from the tests.
```
    imu_msg.angular_velocity.x = data->_gyr[0]/180*3.1415926;
    imu_msg.angular_velocity.y = data->_gyr[1]/180*3.1415926;
   	imu_msg.angular_velocity.z = data->_gyr[2]/180*3.1415926;
    imu_msg.angular_velocity_covariance = {0.01, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.01};
    imu_msg.linear_acceleration.x = data->_acc[0]*g;
  	imu_msg.linear_acceleration.y = data->_acc[1]*g;
  	imu_msg.linear_acceleration.z = data->_acc[2]*g;
    imu_msg.linear_acceleration_covariance = {0.01, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.01};
	imu_pub.publish(imu_msg);
```

# outdoor test

* [video](https://youtu.be/Ev4V4X5OG-M), there's drift when very few features are detected, and the sensor does not come back to starting point.
* [video](https://youtu.be/TvZ5d3nXWMI), after tweaking the parameters to increase the number of features, the drift seems smaller. Launch file is named `noextrinsics`
* [video](https://www.youtube.com/watch?v=ALvl3GCiyn8&feature=youtu.be), another loop with more features in the scene (instead of wall as in above), endpoint drift is smaller.  

# indoor test

* [video](https://www.youtube.com/watch?v=mRJlCelBzfQ&feature=youtu.be), short indoor office scene, using parameters that generate lots of features, drift is reasonably small
* [video](https://www.youtube.com/watch?v=UwGd0sVvYbQ&feature=youtu.be), long office scene, endpoint has large drift from starting position, not as accurate as vins mono using realsense for the same trajectory
* [video](https://youtu.be/4L5F4c4mxGM), another long office scene, same loop, endpoint drift seems to be a bit smaller than before. 
